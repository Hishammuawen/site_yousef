<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        DB::table('roles')->insert([
            $this->create('admin', 'Administrator', 'Administrator Role'),
            $this->create('user', 'User', 'User Role')
        ]);

        DB::commit();
    }

    /**
     * @param $name
     * @param $display_name
     * @param $description
     * @return array
     */
    private function create($name, $display_name, $description)
    {
        return [
            'name' => $name,
            'display_name' => $display_name,
            'description' => $description,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
    }
}
