<?php

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;

class PermissionRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        $admin_permissions = Permission::get();
        $admin_role = Role::findByName('admin');

        $admin_role->perms()->sync($admin_permissions);

        $user_permissions = Permission::where('name', 'LIKE', 'index%')
            ->orWhere('name', 'LIKE', 'show%')
            ->get();
        $user_role = Role::findByName('user');

        $user_role->perms()->sync($user_permissions);

        DB::commit();
    }
}
