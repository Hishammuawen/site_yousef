<?php

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class RoleUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        DB::table('role_user')->insert([
            $this->create(Role::findByName('admin')->id,
                User::where('email', 'admin@boilerplate.com')->first()->id),
        ]);

        DB::commit();
    }

    private function create($role_id, $user_id)
    {
        return [
            'role_id' => $role_id,
            'user_id' => $user_id
        ];
    }
}
