<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_permissions = [
            $this->create('index_users', 'Index Users'),
            $this->create('store_user', 'Store Users'),
            $this->create('show_user', 'Show Users'),
            $this->create('update_user', 'Update Users'),
            $this->create('destroy_user', 'Destroy Users'),
        ];

        $permissions = array_merge($user_permissions);

        DB::beginTransaction();

        DB::table('permissions')->insert($permissions);

        DB::commit();
    }

    /**
     * @param $name
     * @param $display_name
     * @return array
     */
    private function create($name, $display_name)
    {
        return [
            'name' => $name,
            'display_name' => $display_name,
        ];
    }
}
