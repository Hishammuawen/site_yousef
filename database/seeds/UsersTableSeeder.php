<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        DB::table('users')->insert([
            $this->create('admin', 'admin', 'admin@boilerplate.com', 'secret')
        ]);

        DB::commit();
    }

    /**
     * @param $first_name
     * @param $last_name
     * @param $email
     * @param $password
     * @return array
     */
    private function create($first_name, $last_name, $email, $password)
    {
        return [
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'password' => $password,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
    }
}
