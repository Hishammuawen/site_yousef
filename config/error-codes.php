<?php

return [
    'general' => [
        'create_model' => [
            'code' => 1000,
            'message_en' => 'Failed to create Model',
            'message_ar' => 'فشل في إنشاء العنصر'
        ],
        'already_deleted' => [
            'code' => 1001,
            'message_en' => 'Cannot Delete already Deleted Model',
            'message_ar' => 'لا يمكن حذف عنصر محذوف أصلاً',
        ],
        'already_active' => [
            'code' => 1002,
            'message_en' => 'Cannot Activate already Active Model',
            'message_ar' => 'لا يمكن تفعيل عنصر مفعل أصلاً'
        ]
    ],
];
