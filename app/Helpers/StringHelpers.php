<?php

if (!function_exists('arabic_regex')) {

    /**
     * description
     *
     * @param $term
     * @return string
     */
    function arabic_regex($term)
    {
        $a_subs = "(أ|إ|آ|ا)";
        $t_subs = "(ت|ة)";
        $y_subs = "(ي|ى)";

        $regex_map = [
            "أ" => $a_subs,
            "إ" => $a_subs,
            "آ" => $a_subs,
            "ا" => $a_subs,
            "ت" => $t_subs,
            "ة" => $t_subs,
            "ي" => $y_subs,
            "ى" => $y_subs
        ];

        $result = '';

        for ($i = 0; $i < strlen($term); $i++) {
            $letter = mb_substr($term, $i, 1);
            if (isset($regex_map[$letter]))
                $result .= $regex_map[$letter];
            else
                $result .= $letter;
        }

        return $result;
    }
}

if (!function_exists('get_words')) {

    /**
     * description
     *
     * @param $sentence
     * @param int $count
     * @return string
     */
    function get_words($sentence, $count = 30)
    {
        return implode(
            '',
            array_slice(
                preg_split(
                    '/([\s,\.;\?\!]+)/',
                    $sentence,
                    $count * 2 + 1,
                    PREG_SPLIT_DELIM_CAPTURE
                ),
                0,
                $count * 2 - 1
            )
        );
    }
}
