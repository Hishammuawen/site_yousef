<?php

namespace App\Exceptions;

use App\Http\Traits\RestfulRespond;
use BadMethodCallException;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\CssSelector\Exception\InternalErrorException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class Handler extends ExceptionHandler
{
    use RestfulRespond;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param Exception $exception
     *
     * @return void
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param Request $request
     * @param Exception $exception
     *
     * @return Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof NotFoundHttpException) {
            return $this->respondNotFound();
        }

        if ($exception instanceof ModelNotFoundException) {
            return $this->respondNotFound();
        }

        if ($exception instanceof BadMethodCallException) {
            return $this->respondNotFound();
        }

        if ($exception instanceof InternalErrorException) {
            return $this->respondInternalError($exception->getMessage());
        }

        if ($exception instanceof ValidationException) {
            return $this->respondBadRequest($exception->validator->errors()->first());
        }

        if ($exception instanceof BadRequestHttpException) {
            return $this->respondBadRequest($exception->getMessage());
        }

        if ($exception instanceof AppException) {
            return $this->setStatusCode(Response::HTTP_BAD_REQUEST)->respondWithPredefinedError($exception->getMessage());
        }

        if ($exception instanceof MethodNotAllowedHttpException) {
            return $this->respondMethodNotAllowed();
        }

        if ($exception instanceof AuthorizationException) {
            return $exception->getMessage() ? $this->respondNotAuthorized($exception->getMessage()) : $this->respondNotAuthorized();
        }

        if ($exception instanceof UnauthorizedHttpException) {
            return $exception->getMessage() ? $this->respondNotAuthorized($exception->getMessage()) : $this->respondNotAuthorized();
        }

        if ($exception instanceof QueryException) {
            if ($exception->errorInfo[1] == 1062) {
                return $this->respondBadRequest('Unique Constraint Violated!', $message_ar = 'تم انتهاك قيد التفرد!');
            }
        }

        if (env('APP_ENV') !== 'local') {
            return $this->respondInternalError();
        }

        return parent::render($request, $exception);
    }
}
