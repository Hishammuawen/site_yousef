<?php
/**
 * Created by PhpStorm.
 * User: fayez
 * Date: 14/10/18
 * Time: 12:53 م
 */

namespace App\Http\Traits;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

trait SearchableQueryBuilder
{
    /**
     * @var Eloquent $eloquent
     */
    private $eloquent;

    /**
     * @var Builder $inquiry
     */
    private $inquiry;

    private $related;

    /**
     * @param $eloquentType
     * @return $this
     */
    public function useModel($eloquentType)
    {
        $this->eloquent = new $eloquentType();

        return $this;
    }

    /**
     * @param $relations
     * @return $this
     */
    public function useRelations($relations)
    {
        $this->related = $relations;

        return $this;
    }

    /**
     * @param Builder $q
     * @return $this
     */
    public function useQuery(Builder $q)
    {
        $this->inquiry = $q;

        return $this;
    }

    /**
     * @return $this
     */
    public function withEncryption()
    {
        return $this;
    }

    /**
     * @return Builder
     */
    public function buildQuery()
    {
        return $this->inquiry;
    }

    /**
     * @param $columns
     * @return $this
     */
    public function searchModelProperties($columns)
    {
        if (!$this->inquiry || !$this->eloquent) {
            return $this;
        }
        foreach ($columns as $key => $value) {
            if ($value && in_array($key, $this->eloquent->getFillable(), true)) {
                $this->inquiry->where(function (Builder $query) use ($key, $value) {
                    if (ctype_digit($value) || is_bool($value) || is_int($value)) {
                        $query->where($key, $value);
                    } else {
//                            $query->where($key, 'LIKE', "%$value%");
                        $query->where($key, 'REGEXP', '.*' . arabic_regex($value) . '.*');
                    }
                }
                );
            }
        }

        return $this;
    }

    /**
     * @param $columns
     * @return $this|SearchableQueryBuilder
     */
    public function searchModelRelationsProperties($columns)
    {
        if (!$this->inquiry || !$this->eloquent || !$this->related) {
            return $this;
        }
        foreach ($this->related as $relation) {
            $relationFillable = $this->eloquent->{$relation}()
                ->newModelInstance()
                ->getFillable();
            foreach ($columns as $key => $value) {
                $relation_snake_case = Str::snake($relation);
                $filteredKey = str_replace($relation_snake_case . '_', '', $key);
                if ($value !== false &&
                    strpos($key, $relation_snake_case) !== false &&
                    in_array($filteredKey, $relationFillable, true)) {
                    $this->inquiry->whereHas(
                        $relation,
                        function (Builder $q) use ($filteredKey, $value) {
                            if (ctype_digit($value) || is_bool($value) || is_int($value)) {
                                $q->where($filteredKey, $value);
                            } else {
                                $q->where($filteredKey, 'LIKE', "%$value%");
                            }
                        }
                    );
                }
            }
        }

        return $this->searchModelPolymorphicRelationsProperties($columns);
    }

    /**
     * @param $columns
     * @return $this
     */
    private function searchModelPolymorphicRelationsProperties($columns)
    {
        if (!$this->inquiry || !$this->eloquent || !$this->related) {
            return $this;
        }
        $eloquent = $this->eloquent;
        $relations = $this->related;
//        if ($eloquent->{$relation}->morphMap() && is_array($eloquent->{$relation}->morphMap())) {
        $this->inquiry->where(
            function (Builder $q) use ($eloquent, $relations, $columns) {
                foreach ($relations as $relation) {
                    $relation_snake_case = Str::snake($relation);
                    foreach ($eloquent->{$relation}()->morphMap() as $morph_key => $morph_value) {
//                            if (strpos($morph_key, $relation_snake_case) !== false) {
                        $relationFillable = (new $morph_value())->getFillable();
                        $table = (new $morph_value())->getTable();
                        foreach ($columns as $key => $value) {
                            $filteredKey = str_replace($relation_snake_case . '_', '', $key);
                            if ($value !== false &&
//                                        strpos($key, $relation_snake_case) !== false &&
                                in_array($filteredKey, $relationFillable, true)) {

                                $q->whereExists(
                                    function ($q) use (
                                        $eloquent,
                                        $relation_snake_case,
                                        $table,
                                        $filteredKey,
                                        $value
                                    ) {
                                        if (is_int($value) && is_bool($value)) {
                                            $q->select('*')
                                                ->from($eloquent->getTable())
                                                ->join(
                                                    $table,
                                                    $eloquent->getTable() .
                                                    '.' .
                                                    $relation_snake_case .
                                                    '_id',
                                                    '=',
                                                    $table . '.id'
                                                )
                                                ->where($filteredKey, $value);
                                        } else {
                                            $q->select('*')
                                                ->from($eloquent->getTable())
                                                ->join(
                                                    $table,
                                                    $eloquent->getTable() .
                                                    '.' .
                                                    $relation_snake_case .
                                                    '_id',
                                                    '=',
                                                    $table . '.id'
                                                )
                                                ->where($table . '.' . $filteredKey, 'LIKE', "%$value%");
                                        }
                                    }
                                );
                            }
                        }
//                            }
                    }
                }
            }
        );
//        }

        return $this;
    }

    /**
     * @param null $sortBy
     * @param null $sortDirection
     * @return $this
     */
    public function sortModel($sortBy = null, $sortDirection = null)
    {
        if (!$this->inquiry || !$this->eloquent) {
            return $this;
        }

        $columns = Schema::connection($this->eloquent->getConnectionName())->getColumnListing($this->eloquent->getTable());
        if (in_array($this->getSortBy($sortBy), $columns, true) !== false) {
            $this->inquiry->orderBy(
                $this->getSortBy($sortBy), $this->getSortDirection($sortDirection)
            );
        }

        return $this;
    }

    /**
     * @param null $sortBy
     * @return mixed|string|null
     */
    public function getSortBy($sortBy = null)
    {
        return $sortBy ?? request()->get('sortBy') ?? 'created_at';
    }

    /**
     * @param null $sortDir
     * @return mixed|string|null
     */
    public function getSortDirection($sortDir = null)
    {
        return $sortDir ?? request()->get('sortDir') ?? 'desc';
    }
}
