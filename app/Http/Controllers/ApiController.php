<?php

namespace App\Http\Controllers;

use App\Http\Traits\RestfulRespond;
use App\Http\Traits\SearchableQueryBuilder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Routing\Controller as BaseController;

class ApiController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    use RestfulRespond;
    use SearchableQueryBuilder;

    public function __construct()
    {
    }

    /**
     * @param $items
     * @param int $perPage
     * @param null $page
     * @param array $options
     * @return LengthAwarePaginator
     */
    public function paginate($items, $perPage = 10, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    /**
     * @param $model_name_en
     * @param $model_name_ar
     * @return mixed
     */
    public function respondDeletedSuccessfully($model_name_en, $model_name_ar)
    {
        return $this->respondWithMessage(
            $model_name_en . ' Deleted Successfully',
            'تم حذف ' . $model_name_ar . ' بشكل صحيح'
        );
    }
}
