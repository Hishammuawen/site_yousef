<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\ApiController;
use App\Http\Requests\api\Users\UserDestroyRequest;
use App\Http\Requests\api\Users\UserShowRequest;
use App\Http\Requests\api\Users\UsersIndexRequest;
use App\Http\Requests\api\Users\UserStoreRequest;
use App\Http\Requests\api\Users\UserUpdateRequest;
use App\Repositories\api\UserRepository;

class UserController extends ApiController
{
    protected $user_repository;

    public function __construct(UserRepository $user_repository)
    {
        parent::__construct();
        $this->user_repository = $user_repository;
    }

    /**
     * @api {get} users  Index Users
     * @apiPermission index_users
     * @apiName Index Users
     * @apiGroup Users
     * @apiUse UsersIndexSearchParams
     * @apiSuccess {array}   data    All Users Data
     * @apiUse UsersExample
     */
    public function index(UsersIndexRequest $request)
    {
        return $this->respondWithPagination($this->user_repository->searchableIndex($request->validated(),
            $this->getLimit()));
    }

    /**
     * @api {post} users  Store User
     * @apiPermission store_user
     * @apiName Store User
     * @apiGroup Users
     * @apiUse UserStoreParams
     * @apiSuccess {json}   user    Stored User
     * @apiUse UserExample
     */
    public function store(UserStoreRequest $request)
    {
        return $this->respond($this->user_repository->create($request->validated()));
    }

    /**
     * @api {get} users/{id}  Show User
     * @apiPermission show_user
     * @apiName Show User
     * @apiGroup Users
     * @apiSuccess {json}   user    Requested User
     * @apiUse UserExample
     * @apiError (Error 404)    NotFound   When the user is not found
     * @apiuse NotFoundErrorExample
     */
    public function show(UserShowRequest $request, $id)
    {
        return $this->respond($this->user_repository->getById($id));
    }

    /**
     * @api {put} users/{id}  Update User
     * @apiPermission update_user
     * @apiName Update User
     * @apiGroup Users
     * @apiUse UserUpdateParams
     * @apiSuccess {json}   user    Updated User
     * @apiUse UserExample
     */
    public function update(UserUpdateRequest $request, $id)
    {
        return $this->respond($this->user_repository->updateById($id, $request->validated()));
    }

    /**
     * @api {delete} users/{id}  Destroy User
     * @apiPermission destroy_user
     * @apiName Delete User
     * @apiGroup Users
     * @apiSuccess {string}   message    Success Message
     * @apiUse UserDestroySuccessExample
     * @apiError (Error 400)    AlreadyDeleted    When the user is already deleted
     * @apiuse DeleteErrorExample
     * @apiError (Error 404)    NotFound   When the user is not found
     * @apiuse NotFoundErrorExample
     */
    public function destroy(UserDestroyRequest $request, $id)
    {
        if ($this->user_repository->softDeleteById($id)) {
            return $this->respondDeletedSuccessfully('User Deleted Successfully', 'تم حذف المستخدم بنجاح');
        }
    }
}
