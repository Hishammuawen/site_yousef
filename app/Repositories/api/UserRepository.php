<?php


namespace App\Repositories\api;


use App\Models\User;
use App\Repositories\BaseRepository;
use DB;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class UserRepository extends BaseRepository
{

    /**
     * @inheritDoc
     */
    public function model()
    {
        return User::class;
    }

    /**
     * @inheritDoc
     */
    public function relations()
    {
        return [
            'roles'
        ];
    }

    /**
     * @param  array  $data
     * @return User|Model
     * @throws Exception
     */
    public function create(array $data)
    {
        DB::beginTransaction();

        /** @var User $user */
        $user = parent::create($data);

        if (!empty($data['roles'] ?? [])) {
            $user->roles()->sync($data['roles']);
        }

        DB::commit();
        return $user;
    }

    /**
     * @param $id
     * @param  array  $data
     * @param  array  $options
     * @return User|Collection|Model
     * @throws Exception
     */
    public function updateById($id, array $data, array $options = [])
    {
        DB::beginTransaction();

        /** @var User $user */
        $user = parent::updateById($id, $data);

        if (!empty($data['roles'] ?? [])) {
            $user->roles()->sync($data['roles']);
        }

        DB::commit();
        return $user->refresh();
    }
}
