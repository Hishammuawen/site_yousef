<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\Traits\EntrustPermissionTrait;

/**
 * App\Models\Permission
 *
 * @property-read Collection|Role[] $roles
 * @property-read int|null $roles_count
 * @method static Builder|Permission newModelQuery()
 * @method static Builder|Permission newQuery()
 * @method static Builder|Permission query()
 * @mixin Eloquent
 */
class Permission extends Model
{
    use EntrustPermissionTrait;

    public $timestamps = false;

    protected $fillable = [
        'name', 'display_name'
    ];

    protected $hidden = ['pivot'];

    public static function findByName($name)
    {
        return self::where('name', $name)->first();
    }
}
