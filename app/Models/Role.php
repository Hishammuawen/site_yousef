<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\Traits\EntrustRoleTrait;

/**
 * App\Models\Role
 *
 * @property-read Collection|Permission[] $perms
 * @property-read int|null $perms_count
 * @property-read Collection|User[] $users
 * @property-read int|null $users_count
 * @method static Builder|Role newModelQuery()
 * @method static Builder|Role newQuery()
 * @method static Builder|Role query()
 * @mixin Eloquent
 */
class Role extends Model
{
    use EntrustRoleTrait;

    protected $fillable = [
        'name', 'display_name', 'description', 'deleted_at'
    ];

    protected $hidden = ['pivot'];

    public static function findByName($name)
    {
        return self::where('name', $name)->first();
    }
}
