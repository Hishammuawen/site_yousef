<?php

/**
 * @apiDefine UnauthorizedErrorExample
 * @apiErrorExample {json} NotAuthorized
 * HTTP/1.1 401 Unauthorized
 * {
 *     "error": {
 *         "message_en": "Not Authorized!",
 *         "message_ar": "!غير مصرح"
 *     }
 * }
 */

/**
 * @apiDefine NotFoundErrorExample
 * @apiErrorExample {json} NotFound
 * HTTP/1.1 404 Not Found
 * {
 *     "error": {
 *         "message_en": "Not Found!",
 *         "message_ar": "!غير موجود"
 *     }
 * }
 */

/**
 * @apiDefine DeleteErrorExample
 * @apiErrorExample {json} AlreadyDeleted
 * HTTP/1.1 400 Bad Request
 * {
 *     "error": {
 *         "code": 1001,
 *         "message_en": "Cannot Delete already Deleted Model"
 *         "message_ar": "لا يمكن حذف عنصر محذوف أصلاً"
 *     }
 * }
 */

/**
 * @apiDefine ActivateErrorExample
 * @apiErrorExample {json} AlreadyActive
 * HTTP/1.1 400 Bad Request
 * {
 *     "error": {
 *         "code": 1002,
 *         "message_en": "Cannot Activate already Active Model"
 *         "message_ar": "لا يمكن تفعيل عنصر مفعل أصلاً"
 *     }
 * }
 */
