define({
    "name": "Laravel Backend Boilerplate",
    "version": "0.0.0",
    "description": "Laravel Backend Boilerplate apiDoc",
    "title": "Laravel Backend Boilerplate apiDoc",
    "url": "http://127.0.0.1:4444/api/",
    "sampleUrl": false,
    "defaultVersion": "0.0.0",
    "apidoc": "0.3.0",
    "generator": {
        "name": "apidoc",
        "time": "2020-04-07T09:04:32.396Z",
        "url": "http://apidocjs.com",
        "version": "0.19.1"
    }
});
